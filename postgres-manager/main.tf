terraform {
  required_version = ">= 1.0"
  required_providers {
    postgresql = {
      source  = "cyrilgdn/postgresql"
      version = "1.14.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "postgresql" {
  alias           = "db_connection"
  host            = var.host
  port            = var.port
  database        = var.database
  username        = var.connection_username
  password        = var.connection_password
  connect_timeout = var.connect_timeout
  sslmode         = "disable"
}

resource "random_password" "passwords" {
  count            = var.number_of_databases + 1
  length           = 15
  special          = true
  override_special = "_-!?"
  min_special      = 2
}

# Create databases and schemas
resource "postgresql_database" "db" {
  count    = var.number_of_databases
  provider = postgresql.db_connection
  name     = "tf_db_${count.index}"
  owner    = postgresql_role.db_owners[count.index].name
}

# Create databases owners
resource "postgresql_role" "db_owners" {
  count    = var.number_of_databases
  provider = postgresql.db_connection
  name     = "tf_db_owners_role_${count.index}"
  login    = true
  password = random_password.passwords[count.index].result
}

# Create user with read access to all databases
resource "postgresql_role" "all_dbs_read_only" {
  provider = postgresql.db_connection
  name     = "all_dbs_read_only"
  login    = true
  password = random_password.passwords[var.number_of_databases].result
}

resource "postgresql_grant" "readonly_tables_public" {
  count       = var.number_of_databases
  provider    = postgresql.db_connection
  database    = postgresql_database.db[count.index].name
  role        = postgresql_role.all_dbs_read_only.name
  schema      = "public"
  object_type = "table"
  privileges  = ["SELECT"]
}

resource "postgresql_default_privileges" "read_only_tables_default" {
  count       = var.number_of_databases
  provider    = postgresql.db_connection
  database    = postgresql_database.db[count.index].name
  role        = postgresql_role.all_dbs_read_only.name
  owner       = postgresql_database.db[count.index].owner
  object_type = "table"
  privileges  = ["SELECT"]
}
