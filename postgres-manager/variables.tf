variable "host" {
  type        = string
  default     = "127.0.0.1"
  description = "Host of PostgreSQL"
}

variable "port" {
  type        = number
  default     = 5432
  description = "Port of PostgreSQL"
}

variable "database" {
  type        = string
  default     = "postgres"
  description = "Database for PostgreSQL"
}

variable "connection_username" {
  type        = string
  default     = "postgres"
  description = "Username for PostgreSQL connection"
}

variable "connection_password" {
  type        = string
  default     = "postgres"
  description = "Password for PostgreSQL connection"
}

variable "connect_timeout" {
  type        = number
  default     = 15
  description = "Connection timeout in seconds"
}

variable "number_of_databases" {
  type        = number
  default     = 3
  description = "Number of generated databases"
}

//variable "db_owners_passwords" {
//  type        = list(string)
//  default     = []
//  description = "Passwords for databases owners roles"
//}
//variable "read_only_password" {
//  type        = string
//  default     = ""
//  description = "Password for read only database role"
//}
